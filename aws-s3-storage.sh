if [ -z "$AWS_ACCESS_KEY" ]; then
    echo "Need to set AWS_ACCESS_KEY"
    exit 1
fi  

if [ -z "$AWS_REGION" ]; then
    echo "Need to set AWS_REGION"
    exit 1
fi

hal config storage s3 edit \
    --access-key-id $AWS_ACCESS_KEY \
    --secret-access-key \
    --region $AWS_REGION

hal config storage edit --type s3