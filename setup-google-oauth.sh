if [ -z "$CLIENT_ID" ]; then
    echo "Need to set CLIENT_ID"
    exit 1
fi  

if [ -z "$CLIENT_SECRET" ]; then
    echo "Need to set CLIENT_SECRET"
    exit 1
fi

hal config security authn oauth2 edit \
  --client-id $CLIENT_ID \
  --client-secret $CLIENT_SECRET \
  --provider google
  --user-info-requirements hd=gitlab.com
hal config security authn oauth2 enable